
#### checked Timm #### identical

###################################################################################################
#                                                                                                 #
#                                                                                                 #
#     Moisture Index - CLIMEX                                                                     #
#                                                                                                 #
#     - Calculation of Moisture Index(MI)                                                         #
#     - is based on Soil Moisture content (SM)                                                    #
#     - requieres SM-Value and species limits:                                                    #
#     SM0                                                                                         #
#     SM1                                                                                         #
#     SM2                                                                                         #
#     SM3                                                                                         # 
#                                                                                                 #
#                                                                                                 #
###################################################################################################

# function to claculate MI (Moisture Index)
MI.fn <- function(df){
  
  # library for "case_when()"- comment
  require("dplyr")
  
  # MI = Moisture INDEX
  MI <- case_when(
    
    # case 1: SoilMoisture between # SM1 = lower optimum threshold and
    # SM2 = upper optimum threshold 
    # MI = 1 Best case, most suituable
    df$ SM >= df$ SM1 & df$ SM <= df$ SM2 ~ 1,
    
    # case 2: SoilMoisture nor # SM0 = lower threshold and
    # SM3 = upper  threshold 
    # MI = 0 worst case, most unsuituable    
    df$ SM < df$ SM0 | df$ SM > df$ SM3 ~ 0,
    
    # case 3a: SoilMoisture between # SM0 = lower threshold and
    # SM1 = lower optimum threshold
    # intermediate case
    #df$ SM >=  df$ SM0 & df$ SM <  df$ SM1 ~ (df$ SM - df$ SM0)/df$ SM1,
    df$ SM >=  df$ SM0 & df$ SM <  df$ SM1 ~ (df$ SM - df$ SM0)*10,
    
    # case 3b: SoilMoisture above # SM2 = upper optimum threshold and
    # SM3 = upper threshold
    # intermediate case    
    # df$ SM >  df$ SM2 & df$ SM <=  df$ SM3 ~ 1 + ((df$ SM2- df$ SM)/df$ SM3),
    df$ SM >  df$ SM2 & df$ SM <=  df$ SM3 ~ 1 + (df$ SM2- df$ SM)/0.5,
    
    
    # other cases Error
    TRUE ~ NA_real_ )
  
  return(MI)
}




